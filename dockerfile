FROM  alpine:latest
RUN   adduser -S -D -H -h /xmrig miner
RUN   apk add --no-cache --repository http://dl-cdn.alpinelinux.org/alpine/edge/community hwloc-dev
RUN   apk --no-cache upgrade && \
        apk --no-cache add \
        git \
        cmake \
        libuv-dev \
        build-base \
        libressl-dev
RUN   git clone --single-branch --branch dev https://github.com/xmrig/xmrig
WORKDIR    /xmrig/src
RUN sed -i 's/kDefaultDonateLevel = 1/kDefaultDonateLevel = 0/g' donate.h
RUN sed -i 's/kMinimumDonateLevel = 1/kMinimumDonateLevel = 0/g' donate.h
RUN cat donate.h
WORKDIR    /xmrig
RUN   mkdir build
RUN   cmake -DCMAKE_BUILD_TYPE=Release .
RUN   make
RUN   cp xmrig /usr/local/bin
RUN   apk del \
        build-base \
        cmake \
        git
USER miner
ENTRYPOINT  ["./xmrig"]
